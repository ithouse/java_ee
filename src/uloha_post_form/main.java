package uloha_post_form;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by martin.danek on 3/30/17.
 */
public class main {


    public static void main(String [] args) {

        sendPostRequest("www.w3schools.com/html/html_forms.asp");
    }
    public static String sendPostRequest(String url) {

        StringBuffer sb = null;

        try {

            String data = URLEncoder.encode("user", "UTF-8") + "="
                    + URLEncoder.encode("myUserName", "UTF-8") + "&"
                    + URLEncoder.encode("submit", "UTF-8") + "="
                    + URLEncoder.encode("Submit", "UTF-8");


            URL requestUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) requestUrl
                    .openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");

            OutputStreamWriter osw = new OutputStreamWriter(
                    conn.getOutputStream());
            osw.write(data);
            osw.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));

            String in = "";
            sb = new StringBuffer();

            while ((in = br.readLine()) != null) {
                sb.append(in + "\n");
            }

            osw.close();
            br.close();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }

}

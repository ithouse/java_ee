package uloha_streams;

/**
 * Created by martin.danek on 3/9/17.
 */
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class main{

    ///neviem ci to bolo treba ale robil som vsetky ulohy pre 2 rozmerne pole
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int[][] pole = new int [][]{
                {12,2,20,2,2,-1,26,3},
                {1222,2,5,3,-1},
                {3,4,2,-1}
        };

        rozdiel_1(pole);
        pocet_najmensich_2(pole);
        pocet_prime_3(pole, 3);
        intersetion_4(pole);
        transponovane_5(pole);
    }

    public static void rozdiel_1(int[][] pole){
        int sucet_parne = 0;
        int sucet_neparne = 0;

        IntStream stream1 = Arrays.stream(pole).flatMapToInt(x -> Arrays.stream(x));
        sucet_parne = stream1.filter(i-> i %2 == 0 ).sum();
        stream1.close();

        IntStream stream2 = Arrays.stream(pole).flatMapToInt(x -> Arrays.stream(x));
        sucet_neparne = stream2.filter(i-> i %2 != 0 ).sum();
        stream2.close();

        System.out.println("rozdiel neparnych a parnych prvkov je " + String.valueOf(sucet_neparne - sucet_parne));
    }


    public static void pocet_najmensich_2(int[][] pole){
        IntStream stream1 = Arrays.stream(pole).flatMapToInt(x -> Arrays.stream(x));
        final int min = stream1.min().getAsInt();
        stream1.close();

        stream1 = Arrays.stream(pole).flatMapToInt(x -> Arrays.stream(x));
        int pocet = (int)stream1.filter(i -> i <= min).count();
        System.out.println("Toto je minumum: " + min + " a toto je pocet min prvkov: " + pocet);
    }

    public static boolean isPrime(int n) {
        if (n < 2) return false;
        else if (n == 2) return true;
        for (int i = 2; i < Math.pow(n, 0.5) + 1; i++)
            if (n % i == 0)
                return false;
        return true;
    }


    public static void pocet_prime_3(int[][] pole,int n){
        IntStream stream = Arrays.stream(pole).flatMapToInt(x -> Arrays.stream(x));
        int pocet = (int)stream.limit(n).filter(b -> isPrime(b)).count();
        System.out.println("Toto je pocet prvych "+ n +" prvocisel: "+pocet);
    }

    public static  List<Integer> intersetion_4(int [][]pole){
        List<Integer> commons;
        Integer[] now = IntStream.of( pole[0] ).boxed().toArray( Integer[]::new );
        commons =  Arrays.asList(now);

        for(int i=1; i < pole.length;i++){

            now = IntStream.of( pole[i] ).boxed().toArray( Integer[]::new );
            List<Integer> listA = Arrays.asList(now);

            Map<Integer, Long> mapA =
                    listA.stream().collect(
                            Collectors.groupingBy(Integer::intValue, Collectors.counting()));

            List<Integer> commons_current = new ArrayList<>();
            commons.stream()
                    .filter(e -> mapA.get(e) != null)
                    .filter(e -> mapA.get(e) > 0)
                    .forEach(e -> {
                        mapA.put(e, mapA.get(e) - 1);
                        commons_current.add(e);
                    });
            commons = commons_current;
        }
        System.out.println("Toto su spolocne prvy vsetkych poli pola: "+commons);

        return commons;
    }


    //pri roznych dlzkach som dal defaultne prvok 0
    public static <T> int[][] transponovane_5(final int[][] array) {

        final int y_dlzka = Arrays.stream(array).mapToInt(a -> a.length).max().orElse(0);
        final int x_dlzka = array.length;
        final Class<?> componentType = array.getClass().getComponentType().getComponentType();
        final int[][] newArray = (int[][]) Array.newInstance(componentType, y_dlzka, x_dlzka);
        for (int x = 0; x < x_dlzka; x++) {
            for (int y = 0; y < y_dlzka; y++) {
                if (array[x] == null || y >= array[x].length) break;
                newArray[y][x] = array[x][y];
            }
        }
        System.out.println("Toto je transponovane pole: "+Arrays.deepToString(newArray));
        return newArray;
    }

}
